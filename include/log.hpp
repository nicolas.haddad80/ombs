#pragma once

#define LOGERROR(x)                  \
    do                               \
    {                                \
        std::cerr << x << std::endl; \
    } while (0)

#ifndef NDEBUG
#define LOG(x)                       \
    do                               \
    {                                \
        std::cout << x << std::endl; \
    } while (0)
#else
#define LOG(x)
#endif
