#include "http_server.hpp"
#include "served/multiplexer.hpp"

/**
 * @brief this is the server main application
 * it just creating then starting ombs server
 *
 * @return int
 */
int main()
{
  served::multiplexer multiplexer;

  omsb::HttpServer http_server(multiplexer);
  http_server.StartServer();
  return (EXIT_SUCCESS);
}
