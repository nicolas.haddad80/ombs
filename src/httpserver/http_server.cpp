#include "http_server.hpp"

#include "mongodb_handler.hpp"
#include "net/server.hpp"
#include "log.hpp"

namespace omsb
{

  // Constructor
  HttpServer::HttpServer(served::multiplexer multiplexer) : multiplexer(multiplexer)
  {
    InitializeEndpoints();
  }

  // Public methods
  void HttpServer::StartServer()
  {
    served::net::server server(ombsIpAddress, ombsPort, multiplexer);
    std::cout << "Starting server to listen on port " << ombsPort << "..."
              << std::endl;
    server.run(ombsThreads);
  }

  // Private methods
  auto HttpServer::AddMovieToMongoDb()
  {
    return [&](served::response &response, const served::request &request)
    {
      MongoDbHandler mhandler;
      // read the body of request
      json::JSON request_body = json::JSON::Load(request.body());
      LOG(
          " Server Rest API: Request add movie to database movie title = "
          << request_body["movieTitle"].ToString());
      // extract each needed field from the request,  then call MongoDbHandler to add the data
      bool insert_successful = mhandler.AddMovieToDb(request_body["movieTitle"].ToString());

      insert_successful ? served::response::stock_reply(201, response)
                        : served::response::stock_reply(400, response);
    };
  }

  auto HttpServer::GetAllMovies()
  {
    return [&](served::response &response, const served::request &request)
    {
      MongoDbHandler mhandler;
      LOG(" Server Rest API: Request all movies");
      const json::JSON &all_documents = mhandler.GetAllMovies();
      std::ostringstream stream;
      stream << all_documents;
      response << stream.str();
    };
  }

  auto HttpServer::AddTheaterToMongoDb()
  {
    return [&](served::response &response, const served::request &request)
    {
      MongoDbHandler mhandler;
      // read the body of request
      json::JSON request_body = json::JSON::Load(request.body());
      LOG(
          " Server Rest API: Request add theater to database theater name = "
          << request_body["theaterName"].ToString());
      // extract each needed field from the request,  then call MongoDbHandler to add the data
      bool insert_successful = mhandler.AddTheaterToDb(request_body["theaterName"].ToString());

      insert_successful ? served::response::stock_reply(201, response)
                        : served::response::stock_reply(400, response);
    };
  }

  auto HttpServer::GetAllTheaters()
  {
    return [&](served::response &response, const served::request &request)
    {
      MongoDbHandler mhandler;
      LOG(" Server Rest API: Request all theaters");
      const json::JSON &all_documents = mhandler.GetAllTheaters();
      std::ostringstream stream;
      stream << all_documents;
      response << stream.str();
    };
  }

  auto HttpServer::AddMovieShowToMongoDb()
  {
    return [&](served::response &response, const served::request &request)
    {
      MongoDbHandler mhandler;
      // read the body of request
      json::JSON request_body = json::JSON::Load(request.body());
      LOG(
          " Server Rest API: Request add movie show  to database "
          << " theater id = " << request_body["theaterId"].ToString()
          << " movie id = " << request_body["movieId"].ToString());
      // extract each needed field from the request,  then call MongoDbHandler to add the data
      bool insert_successful = mhandler.AddMovieShowToDb(
          request_body["theaterId"].ToString(),
          request_body["movieId"].ToString());

      insert_successful ? served::response::stock_reply(201, response)
                        : served::response::stock_reply(400, response);
    };
  }

  auto HttpServer::GetAllMovieShows()
  {
    return [&](served::response &response, const served::request &request)
    {
      MongoDbHandler mhandler;
      LOG(" Server Rest API: Request all movie shows");
      const json::JSON &all_documents = mhandler.GetAllMovieShows();
      std::ostringstream stream;
      stream << all_documents;
      response << stream.str();
    };
  }

  auto HttpServer::GetTheatersByMovie()
  {
    return [&](served::response &response, const served::request &request)
    {
      MongoDbHandler mhandler;
      LOG(
          " Server Rest API: Request get theater by movie "
          << " movie id = " << request.params["movie_id"]);
      const json::JSON &all_documents = mhandler.GetTheatersByMovie(request.params["movie_id"]);
      std::ostringstream stream;
      stream << all_documents;
      response << stream.str();
    };
  }

  auto HttpServer::GetAvailableSeatsByMovieAndByTheater()
  {
    return [&](served::response &response, const served::request &request)
    {
      MongoDbHandler mhandler;
      LOG(
          " Server Rest API: Request get seats by movie and by theater "
          << "movie_id = " << request.params["movie_id"]
          << " theater_id = " << request.params["theater_id"]);
      const json::JSON &all_documents = mhandler.GetSeatsByMovieAndByTheater(request.params["movie_id"], request.params["theater_id"]);
      std::ostringstream stream;
      stream << all_documents;
      response << stream.str();
    };
  }

  auto HttpServer::BookSeats()
  {
    return [&](served::response &response, const served::request &request)
    {
      MongoDbHandler mhandler;
      json::JSON request_body_json = json::JSON::Load(request.body());
      LOG(
          " Server Rest API: Book seats, seats ids list = "
          << request_body_json.dump());
      if (!BookSeatsRequestBodyValidation(request_body_json))
      {
        served::response::stock_reply(400, response);
      }
      else
      {

        int number_of_seats_to_book = request_body_json.size();
        std::vector<std::string> seats_ids_list{};
        for (int i = 0; i < number_of_seats_to_book; i++)
        {
          std::string seat_id = request_body_json.at(i).ToString();
          if (seat_id.size() != 0)
          {
            seats_ids_list.emplace_back(seat_id);
          }
        }

        bool book_successful = mhandler.BookSeats(seats_ids_list);
        book_successful ? served::response::stock_reply(200, response)
                        : served::response::stock_reply(410, response);
      }
    };
  }

  bool HttpServer::BookSeatsRequestBodyValidation(const json::JSON &request_body_json)
  {
    // TODO robustness: maybe beter validation here if the body is not an array but a simple string.
    int number_of_seats_to_book = request_body_json.size();
    if (request_body_json.dump() == "null" or
        number_of_seats_to_book == 0 or
        request_body_json.hasKey(""))
    {
      std::cerr << "BookSeatsRequestBody malformed " << std::endl;
      return false;
    }
    return true;
  }

  // Public methods
  void HttpServer::InitializeEndpoints()
  {
    multiplexer.handle(ombsAddTheaterEndpoint).post(AddTheaterToMongoDb());
    multiplexer.handle(ombsAddMovieEndpoint).post(AddMovieToMongoDb());
    multiplexer.handle(ombsAddMovieShowEndpoint).post(AddMovieShowToMongoDb());
    multiplexer.handle(ombsBookSeatsEndpoint).post(BookSeats());

    multiplexer.handle(ombsGetAvailableSeatsByMovieAndByTheaterEndpoint).get(GetAvailableSeatsByMovieAndByTheater());
    multiplexer.handle(ombsGetAllTheatersEndpoint).get(GetAllTheaters());
    multiplexer.handle(ombsGetAllMoviesEndpoint).get(GetAllMovies());

    multiplexer.handle(ombsGetTheaterByMovieEndpoint).get(GetTheatersByMovie());
    multiplexer.handle(ombsGetAllMovieShowsEndpoint).get(GetAllMovieShows());
  }

} // namespace omsb
