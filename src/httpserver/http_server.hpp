#pragma once
/*!
* \file http_server.h
* \author Name <nicolas.haddad80@email.com>
* \version 1.0
* \date 04/01/2022
* \brief  this is ombs rest api http server
* \remarks None
*
*
*
*

/*! Importation of librairies*/
#include "SimpleJSON/json.hpp"
#include "multiplexer.hpp"

namespace omsb
{
  // Served parmeters
  constexpr char ombsIpAddress[] = "0.0.0.0";
  constexpr char ombsPort[] = "5000";
  constexpr int ombsThreads = 100;

  // Server Admin API
  constexpr char ombsAddTheaterEndpoint[] = "/addtheater";
  constexpr char ombsAddMovieEndpoint[] = "/addmovie";
  constexpr char ombsGetAllTheatersEndpoint[] = "/getalltheaters";
  constexpr char ombsGetAllMoviesEndpoint[] = "/getallmovies";
  constexpr char ombsAddMovieShowEndpoint[] = "/addshow";
  constexpr char ombsBookSeatsEndpoint[] = "/bookseats";

  // Server user API
  constexpr char ombsGetAllMovieShowsEndpoint[] = "/";
  constexpr char ombsGetTheaterByMovieEndpoint[] = "/{movie_id}";
  constexpr char ombsGetAvailableSeatsByMovieAndByTheaterEndpoint[] = "/{movie_id}/{theater_id}";

  /**
   * @brief
   *
   */

  // TODO robustness: Can handle all false returns by exception thrpown by MogoDbHandler to distanguiche which error is made.
  // TODO robustness: check of all received post requests  bodies that well formed else return 400 (malformed request)
  // TODO robustness: do a test if mongoDB server is shutdown!!!!!!

  /**
   * @brief this the class that handels user REST API requests.
   * we only need to create one object abd start server.
   * there is no need to create multiple instanaces in one node as each request is handlled in dedicated thread.
   *
   */
  class HttpServer
  {
  public:
    /**
     * @brief Construct a new Http Server object.
     *
     * @param multiplexer : served multiplexer.
     */
    HttpServer(served::multiplexer multiplexer);

    /**
     * @brief Starts the server after endpoints have been initialized.
     *
     */
    void StartServer();

  private:
    served::multiplexer multiplexer;

    /**
     * @brief Initializes server endpoints.
     *
     */
    void InitializeEndpoints();

    /**
     * @brief  Callback from served.
     *  this is to add a new theater to backend database.
     *
     *
     * @return auto
     */
    auto AddTheaterToMongoDb();

    /**
     * @brief  Callback from served
     *  this is to add a new movie to backend database.
     *
     * @return auto
     */
    auto AddMovieToMongoDb();

    /**
     * @brief Callback from served.
     *  this is to add a new movie show to backend database.
     *
     * @return auto
     */
    auto AddMovieShowToMongoDb();

    /**
     * @brief Callback from served.
     *  this is to get all present theaters in the  backend database.
     * @return auto
     */
    auto GetAllTheaters();

    /**
     * @brief Callback from served.
     *  this is to get all present movies in the  backend database.
     *
     * @return auto
     */
    auto GetAllMovies();

    /**
     * @brief Callback from served.
     *  this is to get all present movie shows in the  backend database.
     *
     * @return auto
     */
    auto GetAllMovieShows();

    /**
     * @brief Callback from served.
     *  this is to get all theaters showing a particular movie.
     *
     * @return auto
     */
    auto GetTheatersByMovie();

    /**
     * @brief Callback from served.
     *  this is to get all available seats for a given mobvie in aprticular theater.
     *
     * @return auto
     */
    auto GetAvailableSeatsByMovieAndByTheater();

    /**
     * @brief this to book a list of available seats for a show.
     *
     * @return auto
     */
    auto BookSeats();

    /**
     * @brief this is to validate received book seats request boby
     *
     * @param request_body_json
     * @return true : if the request body is well formed.
     * @return false : if the request body is ma lformed.
     */
    bool BookSeatsRequestBodyValidation(const json::JSON &request_body_json);
  }; // Class
} // namespace omsb
