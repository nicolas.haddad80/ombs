#pragma once
/*!
* \file mongodb_handler.h
* \author Name <nicolas.haddad80@email.com>
* \version 1.1
* \date 05/01/2022
* \brief  this is ombs mongo database handler
* \remarks //TODO need to handle robustness by throwing exceptions

/*! Importation of librairies*/
#include <vector>
#include <string>
#include <mutex>

#include "SimpleJSON/json.hpp"

#include "mongocxx/client.hpp"
#include "mongocxx/database.hpp"
#include "mongocxx/uri.hpp"
#include "mongocxx/instance.hpp"

namespace omsb
{

#ifndef NDEBUG
  // for local mongodb (for dev and debug  setups)
  constexpr char ombsMongoDbUri[] = "mongodb://0.0.0.0";
#else
  // for docker-compose production release with mongodb in a docker container
  constexpr char ombsMongoDbUri[] = "mongodb://mongo";

#endif

  constexpr char ombsOmbsDatabaseName[] = "ombs_database";
  constexpr char ombsMovieShowsCollectionName[] = "movieshows";
  constexpr char ombsTheatersCollectionName[] = "theaters";
  constexpr char ombsMoviesCollectionName[] = "movies";
  constexpr char ombsSeatsCollectionName[] = "seats";
  constexpr char ombsBookingsCollectionName[] = "bookings";

  /**
   * @brief Assume that available seats number for a couple of given thrater and movie is 20
   *
   */
  const int NUMBER_OF_SEATS_FOR_A_MOVIE_IN_THEATER = 20;

  /**
   * @brief instance to mongodb driver, should be declared only once in all the application.
   *
   */
  inline mongocxx::instance instance;

  /**
   * @brief this a lock mutex to avoid averbooking when we book a list of seats
   *
   */
  inline std::mutex seats_booking_mutex_lock;

  /**
   * @brief this is the class to allow creation of controller objects as needed for ombs mongodb database
   *
   */
  class MongoDbHandler
  {
  public:
    /**
     * @brief Construct a new Mongo Db Handler object
     *
     */
    MongoDbHandler();

    /**
     * @brief creates a new theater in ombs database
     *
     * @param theater_name : the desired theater name for the new theater to create
     * @return true : if the theater has been successfuly created
     * @return false : if the theater creation is not possible
     */
    bool AddTheaterToDb(const std::string &theater_name);

    /**
     * @brief creates a new movie in ombs database.
     *
     * @param movie_title
     * @return true : if the movie has been successfuly created.
     * @return false : if the movie creation is not possible.
     */
    bool AddMovieToDb(const std::string &movie_title);

    /**
     * @brief Get all present theaters in the database
     *
     * @return json::JSON : json file representing a list of all theaters in the database.
     */
    json::JSON GetAllTheaters();

    /**
     * @brief Get all present movies in the database
     *
     * @return json::JSON : json file representing a list of all movies in the database.
     */
    json::JSON GetAllMovies();

    /**
     * @brief creates a new movie show in ombs database.
     *  By default, 20 avaialble seats are created for a given couple of movie and theater.
     *
     * @param theater_id : theater id where the movie show will occure.
     * @param movie_id : movie id that will be shown.
     * @return true : if the show has been successfuly created.
     * @return false : if the show has not been created.
     */
    bool AddMovieShowToDb(const std::string &theater_id, const std::string &movie_id);

    /**
     * @brief Get the All Movie Shows that has at least one avaialbale seat for booking.
     *
     * @return json::JSON : json file representing a list of all available movie shows
     *  that have at elast one availabe seat to book in the database.
     */
    json::JSON GetAllMovieShows();

    /**
     * @brief Get all Theaters that are showing a particular movie.
     *
     * @param movie_id : movie id wanted by user.
     * @return json::JSON : json file representing a list of all theaters showing a particular movie.
     */
    json::JSON GetTheatersByMovie(const std::string &movie_id);

    /**
     * @brief Get all availabe seats fior a given movie in given theater.
     *
     * @param movie_id : movie id wanted by user.
     * @param theater_id : theater id wanted by user.
     * @return json::JSON
     */
    json::JSON GetSeatsByMovieAndByTheater(const std::string &movie_id, const std::string &theater_id);

    /**
     * @brief allow the user to book a list of available seats.
     *
     * @param seatsIdsList : list of desired seats id.
     * @return true : if all desired seats have been successfuly booked.
     * @return false : if at least one desired seats is not more available
     *                 or does not exists,
     *                 or all seats does not belong to same movie show.
     */
    bool BookSeats(const std::vector<std::string> &seatsIdsList);

    /**
     * @brief used by admin user to reset datbase.
     *
     * @return true : if the database has been successfuly reseted.
     * @return false : if teh database can not be reseted.
     */
    bool DropDatabase();

  private:
    mongocxx::uri uri;
    mongocxx::client client;
    mongocxx::database db;

    /**
     * @brief
     *
     * @param seat_id
     * @param show_id
     * @param booking_id
     * @return true
     * @return false
     */
    bool BookSingleSeat(const std::string &seat_id, std::string &show_id, const std::string &booking_id);

    /**
     * @brief
     *
     * @param seat_id
     * @return std::string
     */
    std::string FindShowIdBySeat(const std::string &seat_id);

    /**
     * @brief
     *
     * @param seat_id
     * @param book_id
     * @return true
     * @return false
     */
    bool InsertSeatToBooking(const std::string &seat_id, const std::string &book_id);

    /**
     * @brief Create a Booking object
     *
     * @param show_id
     * @param user_email
     * @return std::string
     */
    std::string CreateBooking(const std::string &show_id, const std::string &user_email = "nicolas.haddad80@gmail.com");

  }; // Class
} // namespace omsb
