#include "mongodb_handler.hpp"

#include "bsoncxx/builder/stream/document.hpp"
#include "bsoncxx/json.hpp"
#include "log.hpp"

namespace omsb
{
    // Constructor (public)
    MongoDbHandler::MongoDbHandler() : uri(mongocxx::uri(ombsMongoDbUri)),
                                       client(mongocxx::client(uri)),
                                       db(client[ombsOmbsDatabaseName])
    {
        // TODO data consistence: configure all collections to have unique good id, movies by title, thrater by name, shows by couple of movie and theater ids.
    }

    // public Methods:
    bool MongoDbHandler::AddMovieToDb(const std::string &movie_title)
    {
        LOG("adding a new movie"
            << " movie title = " << movie_title);
        mongocxx::collection collection = db[ombsMoviesCollectionName];
        auto builder = bsoncxx::builder::stream::document{};
        bsoncxx::v_noabi::document::value doc_value =
            builder << "movieTitle" << movie_title << bsoncxx::builder::stream::finalize;

        try
        {
            bsoncxx::stdx::optional<mongocxx::result::insert_one> maybe_result =
                collection.insert_one(doc_value.view());
            if (maybe_result)
            {
                return maybe_result->inserted_id().get_oid().value.to_string().size() != 0;
            }
        }
        catch (const std::exception &e)
        {
            std::cerr << "Exception occured" << std::endl;
            std::cerr << e.what() << std::endl;
            return false;
        }
        return false;
    }

    json::JSON MongoDbHandler::GetAllMovies()
    {
        mongocxx::collection collection = db[ombsMoviesCollectionName];
        mongocxx::cursor cursor = collection.find({});
        json::JSON result;
        result["movies"] = json::Array();
        if (cursor.begin() != cursor.end())
        {
            for (auto doc : cursor)
            {
                result["movies"].append(json::JSON::Load(bsoncxx::to_json(doc)));
            }
        }
        return result;
    }

    bool MongoDbHandler::AddTheaterToDb(const std::string &theater_name)
    {
        LOG(
            "adding a new theater"
            << " theater Name = " << theater_name);

        mongocxx::collection collection = db[ombsTheatersCollectionName];
        auto builder = bsoncxx::builder::stream::document{};

        bsoncxx::v_noabi::document::value doc_value =
            builder << "theaterName" << theater_name << bsoncxx::builder::stream::finalize;

        try
        {
            bsoncxx::stdx::optional<mongocxx::result::insert_one> maybe_result =
                collection.insert_one(doc_value.view());
            if (maybe_result)
            {
                return maybe_result->inserted_id().get_oid().value.to_string().size() != 0;
            }
            else
            {
                return false;
            }
        }
        catch (const std::exception &e)
        {
            return false;
        }
    }

    json::JSON MongoDbHandler::GetAllTheaters()
    {
        mongocxx::collection collection = db[ombsTheatersCollectionName];
        mongocxx::cursor cursor = collection.find({});
        json::JSON result;
        result["theaters"] = json::Array();
        if (cursor.begin() != cursor.end())
        {
            for (auto doc : cursor)
            {

                result["theaters"].append(json::JSON::Load(bsoncxx::to_json(doc)));
            }
        }
        return result;
    }

    bool MongoDbHandler::AddMovieShowToDb(const std::string &theater_id, const std::string &movie_id)
    {
        LOG(
            " toto adding a new movie show "
            << " theater Id = " << theater_id
            << " movie Id = " << movie_id);
        mongocxx::collection collection = db[ombsMovieShowsCollectionName];
        auto builder = bsoncxx::builder::stream::document{};
        try
        {
            bsoncxx::oid movie_object_id(movie_id);
            builder << "movieId" << movie_object_id;
        }
        catch (const std::exception &e)
        {
            LOGERROR(
                "this id is not a valide theater Id"
                << e.what());
            return false;
        }

        try
        {
            bsoncxx::oid theater_object_id(theater_id);
            builder << "theaterId" << theater_object_id;
        }
        catch (const std::exception &e)
        {
            LOGERROR("this id is not a valide theater Id " << e.what());
            return false;
        }

        auto seats_array = builder << "availableSeatsIds" << bsoncxx::builder::stream::open_array;

        // Creating seats for this show
        mongocxx::collection seats_collection = db[ombsSeatsCollectionName];

        for (int i = 1; i <= NUMBER_OF_SEATS_FOR_A_MOVIE_IN_THEATER; i++)
        {
            auto seat_builder = bsoncxx::builder::stream::document{};
            bsoncxx::v_noabi::document::value seat_doc_value =
                seat_builder << "seatName"
                             << "S" + std::to_string(i)
                             << "seatStatus"
                             << "available"
                             << bsoncxx::builder::stream::finalize;
            try
            {
                bsoncxx::stdx::optional<mongocxx::result::insert_one> maybe_result =
                    seats_collection.insert_one(seat_doc_value.view());
                if (maybe_result)
                {
                    seats_array = seats_array << maybe_result->inserted_id().get_oid().value.to_string();
                }
                else
                {
                    return false;
                }
            }
            catch (const std::exception &e)
            {
                return false;
            }
        }
        auto final_seat_array = seats_array << bsoncxx::builder::stream::close_array;

        bsoncxx::v_noabi::document::value doc_value =
            final_seat_array << bsoncxx::builder::stream::finalize;

        LOG(
            " Created seats igs for the show are: "
            << bsoncxx::to_json(doc_value));

        try
        {
            bsoncxx::stdx::optional<mongocxx::result::insert_one> maybe_result =
                collection.insert_one(doc_value.view());
            if (maybe_result)
            {
                return maybe_result->inserted_id().get_oid().value.to_string().size() != 0;
            }
            else
            {
                LOGERROR(" Error while trying to insert a movie show");
                return false;
            }
        }
        catch (const std::exception &e)
        {
            LOGERROR(e.what());
            return false;
        }
        return false;
    }

    json::JSON MongoDbHandler::GetAllMovieShows()
    {
        mongocxx::collection shows_collection = db[ombsMovieShowsCollectionName];
        json::JSON result;
        mongocxx::pipeline pipe{};

        pipe.match(
            bsoncxx::builder::basic::make_document(
                bsoncxx::builder::basic::kvp("seats", bsoncxx::builder::basic::make_document(bsoncxx::builder::basic::kvp("$ne", bsoncxx::builder::basic::array{})))));

        pipe.lookup(
            bsoncxx::builder::basic::make_document(
                bsoncxx::builder::basic::kvp("from", ombsMoviesCollectionName),
                bsoncxx::builder::basic::kvp("localField", "movieId"),
                bsoncxx::builder::basic::kvp("foreignField", "_id"),
                bsoncxx::builder::basic::kvp("as", "movie")));

        pipe.project(
            bsoncxx::builder::basic::make_document(
                bsoncxx::builder::basic::kvp("movie.movieTitle", 1),
                bsoncxx::builder::basic::kvp("movie._id", 1),
                bsoncxx::builder::basic::kvp("_id", 0)));

        pipe.unwind("$movie");

        pipe.group(
            bsoncxx::builder::basic::make_document(
                bsoncxx::builder::basic::kvp("_id", "movie"),
                bsoncxx::builder::basic::kvp("shows", bsoncxx::builder::basic::make_document(
                                                          bsoncxx::builder::basic::kvp("$addToSet", "$movie")))));

        pipe.project(
            bsoncxx::builder::basic::make_document(
                bsoncxx::builder::basic::kvp("shows", 1),
                bsoncxx::builder::basic::kvp("_id", 0)));

        auto cursor = shows_collection.aggregate(pipe, mongocxx::options::aggregate{});

        result = json::Object();

        if (cursor.begin() != cursor.end())
        {
            for (auto doc : cursor)
            {
                result = json::JSON::Load(bsoncxx::to_json(doc));
            }
        }
        return result;
    }

    json::JSON MongoDbHandler::GetTheatersByMovie(const std::string &movie_id)
    {

        LOG(
            "Database handler: looking for theaters with available seats showing movie_id : "
            << movie_id);
        mongocxx::collection shows_collection = db[ombsMovieShowsCollectionName];
        json::JSON result;
        mongocxx::pipeline pipe{};
        bsoncxx::oid movie_object_id(movie_id);
        pipe.match(
            bsoncxx::builder::basic::make_document(
                bsoncxx::builder::basic::kvp("movieId", movie_object_id),
                bsoncxx::builder::basic::kvp("seats", bsoncxx::builder::basic::make_document(bsoncxx::builder::basic::kvp("$ne", bsoncxx::builder::basic::array{})))));

        pipe.lookup(
            bsoncxx::builder::basic::make_document(
                bsoncxx::builder::basic::kvp("from", ombsTheatersCollectionName),
                bsoncxx::builder::basic::kvp("localField", "theaterId"),
                bsoncxx::builder::basic::kvp("foreignField", "_id"),
                bsoncxx::builder::basic::kvp("as", "theater")));

        pipe.project(
            bsoncxx::builder::basic::make_document(
                bsoncxx::builder::basic::kvp("theater.theaterName", 1),
                bsoncxx::builder::basic::kvp("theater._id", 1),
                bsoncxx::builder::basic::kvp("_id", 0)));

        pipe.unwind("$theater");

        pipe.project(
            bsoncxx::builder::basic::make_document(
                bsoncxx::builder::basic::kvp("theater._id", 1),
                bsoncxx::builder::basic::kvp("theater.theaterName", 1)));

        auto cursor = shows_collection.aggregate(pipe, mongocxx::options::aggregate{});

        result = json::Array();

        if (cursor.begin() != cursor.end())
        {
            for (auto doc : cursor)
            {
                result.append(json::JSON::Load(bsoncxx::to_json(doc)));
            }
        }
        return result;
    }

    json::JSON MongoDbHandler::GetSeatsByMovieAndByTheater(const std::string &movie_id, const std::string &theater_id)
    {

        LOG(
            " Database handler: looking for available seats for movie_id : "
            << movie_id << "and theaer_id : " << theater_id);

        mongocxx::collection shows_collection = db[ombsMovieShowsCollectionName];
        json::JSON result;
        mongocxx::pipeline pipe{};

        // TODO Mandatory : for robustness add try catch
        bsoncxx::oid movie_object_id(movie_id);
        bsoncxx::oid theater_object_id(theater_id);

        pipe.match(
            bsoncxx::builder::basic::make_document(
                bsoncxx::builder::basic::kvp("movieId", movie_object_id),
                bsoncxx::builder::basic::kvp("theaterId", theater_object_id),
                bsoncxx::builder::basic::kvp("availableSeatsIds", bsoncxx::builder::basic::make_document(bsoncxx::builder::basic::kvp("$ne", bsoncxx::builder::basic::array{})))));

        pipe.project(
            bsoncxx::builder::basic::make_document(
                bsoncxx::builder::basic::kvp("availableSeatsIds", 1),
                bsoncxx::builder::basic::kvp("_id", 0)));

        auto cursor = shows_collection.aggregate(pipe, mongocxx::options::aggregate{});

        result = json::Object();

        if (cursor.begin() != cursor.end())
        {
            for (auto doc : cursor)
            {
                result = json::JSON::Load(bsoncxx::to_json(doc));
            }
        }
        return result;
    }

    bool MongoDbHandler::BookSeats(const std::vector<std::string> &seatsIdsList)
    {
        // Check that we have at least one seat to book
        if (seatsIdsList.size() <= 0)
        {
            return false;
        }

        std::string first_seat_id = seatsIdsList[0];
        std::string show_id = FindShowIdBySeat(first_seat_id);

        // Looking for coresponding show_id for tfirst seat_id
        if (!show_id.size())
        {
            return false;
        }

        // Check that precondition of all seats_ids belongs to the show_id of the first seat_id and no over booking for same seat
        for (const auto &seat_id : seatsIdsList)
        {
            if (FindShowIdBySeat(seat_id) != show_id)
            {
                std::cerr << "seat_id : " << seat_id << " not found in the show" << std::endl;
                return false;
            }

            if (count(seatsIdsList.begin(), seatsIdsList.end(), seat_id) != 1)
            {
                std::cerr << "can not book seat_id : " << seat_id << " more than one time" << std::endl;
                return false;
            }
        }

        std::vector<std::string> successful_booked_seats_ids_list{};
        bool all_seats_booked = true;

        // Creating a booking
        std::string booking_id = CreateBooking(show_id);
        LOG(
            "Booking Created booking_id = "
            << booking_id);

        // Booking seat, this is critical section acquire lock
        seats_booking_mutex_lock.lock();
        for (const auto &seat_id : seatsIdsList)
        {
            if (BookSingleSeat(seat_id, show_id, booking_id))
            {
                successful_booked_seats_ids_list.emplace_back(seat_id);
            }
            else
            {
                all_seats_booked = false;
                break;
            }
        }
        // Release lock
        seats_booking_mutex_lock.unlock();

        LOG(
            "number of successfuly booked seats = "
            << std::to_string(successful_booked_seats_ids_list.size()));

        return all_seats_booked;
    }

    bool MongoDbHandler::DropDatabase()
    {
        db.drop();
        return true;
    }

    // private methods
    bool MongoDbHandler::BookSingleSeat(const std::string &seat_id, std::string &show_id, const std::string &booking_id)
    {

        mongocxx::collection collection = db[ombsMovieShowsCollectionName];

        auto builder = bsoncxx::builder::stream::document{};
        bsoncxx::oid show_object_id(show_id);
        bsoncxx::document::value query_doc =
            builder << "_id" << show_object_id
                    << bsoncxx::builder::stream::finalize;

        bsoncxx::document::value remove_doc =
            builder << "$pull" << bsoncxx::builder::stream::open_document << "availableSeatsIds"
                    << seat_id << bsoncxx::builder::stream::close_document
                    << bsoncxx::builder::stream::finalize;

        bsoncxx::stdx::optional<mongocxx::result::update> maybe_result =
            collection.update_one(query_doc.view(), remove_doc.view());

        if (maybe_result && maybe_result->modified_count() == 1)
        {
            // inserting booked seat to the booking
            InsertSeatToBooking(seat_id, booking_id);
            return true;
        }
        else
        {
            return false;
        }
    }

    std::string MongoDbHandler::FindShowIdBySeat(const std::string &seat_id)
    {
        mongocxx::collection collection = db[ombsMovieShowsCollectionName];
        auto builder = bsoncxx::builder::stream::document{};
        bsoncxx::document::value query_doc =
            builder << "availableSeatsIds" << seat_id
                    << bsoncxx::builder::stream::finalize;

        mongocxx::cursor cursor = collection.find(query_doc.view());

        json::JSON result = json::Array();
        if (cursor.begin() != cursor.end())
        {
            for (auto doc : cursor)
            {
                result.append(json::JSON::Load(bsoncxx::to_json(doc)));
            }
            // extracting showId
            const std::string show_id_key = "_id";
            const std::string show_oid_key = "$oid";
            if (result.at(0).hasKey(show_id_key))
            {
                return result.at(0).at(show_id_key).at(show_oid_key).ToString();
            }
            else
            {
                return "";
            }
        }
        else
        {
            return "";
        }
    }

    bool MongoDbHandler::InsertSeatToBooking(const std::string &seat_id, const std::string &book_id)
    {
        mongocxx::collection collection = db[ombsBookingsCollectionName];
        auto builder = bsoncxx::builder::stream::document{};
        bsoncxx::oid document_id(book_id);

        bsoncxx::document::value query_doc =
            builder << "_id" << document_id
                    << bsoncxx::builder::stream::finalize;
        bsoncxx::document::value update_doc =
            builder << "$addToSet" << bsoncxx::builder::stream::open_document << "bookedSeatsIds"
                    << seat_id << bsoncxx::builder::stream::close_document
                    << bsoncxx::builder::stream::finalize;

        bsoncxx::stdx::optional<mongocxx::result::update> maybe_result =
            collection.update_one(query_doc.view(), update_doc.view());

        if (maybe_result)
        {
            return maybe_result->modified_count() == 1;
        }
        return false;
    }

    std::string MongoDbHandler::CreateBooking(const std::string &show_id, const std::string &user_email)
    {

        mongocxx::collection collection = db[ombsBookingsCollectionName];

        auto builder = bsoncxx::builder::stream::document{};

        bsoncxx::v_noabi::document::value doc_value =
            builder << "showId" << show_id << "userEmail" << user_email << bsoncxx::builder::stream::finalize;

        try
        {
            bsoncxx::stdx::optional<mongocxx::result::insert_one> maybe_result =
                collection.insert_one(doc_value.view());

            if (maybe_result)
            {
                std::string created_booking_id = maybe_result->inserted_id().get_oid().value.to_string();
                if (created_booking_id.size() != 0)
                {
                    return created_booking_id;
                }
                else
                {
                    return "";
                }
            }
        }
        catch (const std::exception &e)
        {
            LOGERROR(
                " error during booking creation "
                << e.what());
            return "";
        }
        return "";
    }
} // namespace oms
