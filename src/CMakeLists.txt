add_executable(ombsserver main.cpp)

add_subdirectory(httpserver)
add_subdirectory(mongodbhandler)

target_link_libraries(ombsserver
  PRIVATE
    httpserver
    served
    pthread
    mongodbhandler
    mongocxx
    bsoncxx
    boost_system
  )
