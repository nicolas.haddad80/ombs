#!/bin/bash

function install_mongocxx_dependencies() {
  TOP_DIR=$(pwd)
  wget https://github.com/mongodb/mongo-c-driver/releases/download/1.20.0/mongo-c-driver-1.20.0.tar.gz
  tar xzf mongo-c-driver-1.20.0.tar.gz
  cd mongo-c-driver-1.20.0
  mkdir cmake-build
  cd cmake-build
  cmake -DENABLE_AUTOMATIC_INIT_AND_CLEANUP=OFF ..
  make 
  make install
  cd $TOP_DIR

  wget https://github.com/mongodb/mongo-cxx-driver/releases/download/r3.6.6/mongo-cxx-driver-r3.6.6.tar.gz
  tar xzf mongo-cxx-driver-r3.6.6.tar.gz
  cd mongo-cxx-driver-r3.6.6
  # mkdir build
  cd build
  cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr/local

  make
  make install
  cd $TOP_DIR
  rm -rf mongo-c-driver-1.20.0
  rm -rf mongo-cxx-driver-r3.6.6
  rm mongo-cxx-driver-r3.6.6.tar.gz
  rm mongo-c-driver-1.20.0.tar.gz
}

function main() {
  install_mongocxx_dependencies
}

main