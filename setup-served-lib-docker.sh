#!/bin/bash

function install_served_dependencies() {
  TOP_DIR=$(pwd)
  git clone https://github.com/meltwater/served.git
  mkdir served.build
  cd served.build
  cmake ../served
  make 
  make install
  cd $TOP_DIR
  rm -rf served
  rm -rf served.build
}

function main() {
  install_served_dependencies
}

main
