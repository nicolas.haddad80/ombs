FROM ubuntu:20.04
ARG DEBIAN_FRONTEND=noninteractive

# Copy the current folder which contains C++ source code to the Docker image under /usr/src
COPY . /usr/src/ombs
# install project dependencies

RUN apt-get update -y
RUN apt install -y \
pkg-config \
libssl-dev \
libsasl2-dev \
libbson-dev \
git \
curl \
build-essential \
libmongoc-1.0-0 \
libboost-system-dev \
libboost-all-dev \
python \
cmake \
ragel \
wget \
libgtest-dev \
lcov \
nodejs \
npm \
libmongoc-1.0-0 \
doxygen \
doxygen-gui \
graphviz

# install rest api auto testting and  reporting tools
RUN npm install -g newman
RUN npm install -g newman-reporter-htmlextra

# build and install served library
RUN /usr/src/ombs/setup-served-lib-docker.sh

# build and install mongocxx driver to use mongoDB 
RUN /usr/src/ombs/setup-mongocxx-lib-docker.sh

# build and install google tests suite
# RUN /usr/src/ombs/setup-google-tests-docker.sh

# Specify the working directory
WORKDIR /usr/src/ombs

# build ombs debug version
RUN /usr/src/ombs/build-all.sh

EXPOSE 5000

# run server
RUN ["chmod", "+x", "/usr/src/ombs/run-ombsserver-inside-docker.sh"]
CMD /usr/src/ombs/run-ombsserver-inside-docker.sh 
