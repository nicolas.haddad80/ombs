#!/bin/bash

sudo  apt update -y

sudo apt install -y \
pkg-config \
libssl-dev \
libsasl2-dev \
libbson-dev \
git \
curl \
build-essential \
libboost-system-dev \
libboost-all-dev \
libmongoc-1.0-0 \
python \
cmake \
ragel \
wget \
libgtest-dev \
lcov \
doxygen \
doxygen-gui \
graphviz \
nodejs \
npm

    # node-gyp \
sudo npm install -g newman
sudo npm install -g newman-reporter-htmlextra

# build and install cmake 3.20.0
# ./setup-cmake-latest.sh

# build and install mongocxx driver to use mongoDB 
./setup-mongocxx-lib.sh

# build and install served library
./setup-served-lib.sh

# build and install google tests suite
./setup-google-tests.sh

# Install local mongodb database
#TODO: replace by the one for 20.04 or f docker container milestone reanched no need to install local mongodb
# ./setup-mongodb-18.06.sh
