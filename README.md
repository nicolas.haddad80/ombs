#author: 
0.1. Nicolas HADDAD: nicolas.haddad80@gmail.com
---
- [1. OMBS : Online Movie show Booking Service](#1-ombs--online-movie-show-booking-service)
  - [1.1. How to build](#11-how-to-build)
    - [1.1.1. Docker hub image use instead of building](#111-docker-hub-image-use-instead-of-building)
    - [1.1.2. Build docker-compose image](#112-build-docker-compose-image)
      - [1.1.2.1. for linux](#1121-for-linux)
      - [1.1.2.2. for windows](#1122-for-windows)
      - [1.1.2.3. for all platformes that supports docker](#1123-for-all-platformes-that-supports-docker)
    - [1.1.3. Ubuntu development environement](#113-ubuntu-development-environement)
      - [1.1.3.1. Setup environment](#1131-setup-environment)
      - [1.1.3.2. Debug build](#1132-debug-build)
      - [1.1.3.3. release build](#1133-release-build)
      - [1.1.3.4. all builds (both debug and release)](#1134-all-builds-both-debug-and-release)
      - [1.1.3.5. How to tests and generate test reports](#1135-how-to-tests-and-generate-test-reports)
  - [1.2. Design and code doumentation](#12-design-and-code-doumentation)
  - [1.3. How to run](#13-how-to-run)
    - [1.3.1. run debug version](#131-run-debug-version)
    - [1.3.2. run release version](#132-run-release-version)
  - [1.4. How to use](#14-how-to-use)
  - [1.5. OMBS API Documentation](#15-ombs-api-documentation)
    - [1.5.1. Get all playing movie shows with at least one available seat](#151-get-all-playing-movie-shows-with-at-least-one-available-seat)
      - [1.5.1.1. request example](#1511-request-example)
    - [1.5.2. Get all theaters that showing a particular movie and having at least one available seat](#152-get-all-theaters-that-showing-a-particular-movie-and-having-at-least-one-available-seat)
      - [1.5.2.1. Mandatory path parameters](#1521-mandatory-path-parameters)
      - [1.5.2.2. request example](#1522-request-example)
    - [1.5.3. Get all available seats for a given movie in a given theater](#153-get-all-available-seats-for-a-given-movie-in-a-given-theater)
      - [1.5.3.1. Mandatory path parameters](#1531-mandatory-path-parameters)
      - [1.5.3.2. request example](#1532-request-example)
    - [1.5.4. Get all present movies](#154-get-all-present-movies)
      - [1.5.4.1. request example](#1541-request-example)
      - [1.5.4.2. Response example](#1542-response-example)
    - [1.5.5. Get all present theaters](#155-get-all-present-theaters)
      - [1.5.5.1. request example](#1551-request-example)
      - [1.5.5.2. Response example](#1552-response-example)
    - [1.5.6. Add a new movie](#156-add-a-new-movie)
      - [1.5.6.1. Mandatory body parameters](#1561-mandatory-body-parameters)
      - [1.5.6.2. Body example](#1562-body-example)
      - [1.5.6.3. Request example](#1563-request-example)
    - [1.5.7. Add a new tehater](#157-add-a-new-tehater)
      - [1.5.7.1. Mandatory body parameters](#1571-mandatory-body-parameters)
      - [1.5.7.2. Body example](#1572-body-example)
      - [1.5.7.3. Request example](#1573-request-example)
    - [1.5.8. Add movie show](#158-add-movie-show)
      - [1.5.8.1. Mandatory body parameters](#1581-mandatory-body-parameters)
      - [1.5.8.2. Body example](#1582-body-example)
      - [1.5.8.3. Request example](#1583-request-example)
    - [1.5.9. Book show seats](#159-book-show-seats)
      - [1.5.9.1. Mandatory body parameters](#1591-mandatory-body-parameters)
      - [1.5.9.2. Body example](#1592-body-example)
      - [1.5.9.3. Request example](#1593-request-example)

# 1. OMBS : Online Movie show Booking Service

ombs stands for Online Movie Booking Service, this is a back end application service that can be used to serve

a simple CLI, GUI or API for web services.

## 1.1. How to build

first clone git repository and go insode the clonned directory

```bash
git clone https://gitlab.com/nicolas.haddad80/ombs.git
cd ombs
```  

Now you need to add execution permession to all .sh script by issuing the following command

```bash
sudo chmod +x *.sh
```

you can now simply build with docker image and deploy it as followning, or follow the sections after to build in linux ubuntu distibution

### 1.1.1. Docker hub image use instead of building

First, you need to install docker and docker-compose apps on your linux,  windows , mac or any other platform that supports docker.  
A docker-compose image is availabe for pull on docker hub at thins location: nicolashaddad/ombs:1.0.0  
To use it follow those commands raspecting the order.  
Those commands are same and supported on all platforms that support docker  
A mandatory prerequist that you need to satisfy is to be sure that you have set docker volu as data pesistency is supported.

```bash
docker pull nicolashaddad/ombs:1.0.0

docker-compose up -d mongo

docker-compose up -d ombs

```

### 1.1.2. Build docker-compose image

If you need to generate another docker-compose image ratehr than using the available one from docker hub (if you are a contirubito for instance), please follow those guide lines:  

First, you need to install docker and docker-compose apps on your linux,  windows , mac or any other platform that supports docker. 

#### 1.1.2.1. for linux

In a terminal issue following commands:

 ```bash
sudo apt install docker    
sudo apt  install docker-compose

 ```

You can now build docker image by issuing this command  

 ```bash
sudo ./build-docker-image.sh

 ```

finaly, you can now run ombs server by issuing this command  

```bash
docker-compose up -d mongo
docker-compose up -d ombs

```

#### 1.1.2.2. for windows

We assume that you have installed docker and docker-compose prerequists.  
To build docker-compose image pelase open a windows power shell terminal and go to ombs main directory, now issue following command  

 ``` powershell
.\windows-power-shell-build-docker-image.ps1

 ```

finaly, you can now run ombs server by issuing this command  

```bash
docker-compose up -d mongo
docker-compose up -d ombs
```

#### 1.1.2.3. for all platformes that supports docker

Following docker-compose dommands works for anay platform that supports  docker  

```bash
docker-compose build
docker-compose up -d mongo
docker-compose up -d ombs
```  

### 1.1.3. Ubuntu development environement

First you need to setup your build environment, For this, please follow the next section.

#### 1.1.3.1. Setup environment

A shell script is available to install all needed tools for development.  
Pelase run it as follonwing before starting any build.

```bash
sudo ./setup-development-env.sh
```

#### 1.1.3.2. Debug build

```bash
./build-debug.sh
```

#### 1.1.3.3. release build

```bash
./build-release.sh
```

#### 1.1.3.4. all builds (both debug and release)

```bash
./build-all.sh
- ```

#### clean all builds env

```bash
./clean-all-builds.sh
```

#### 1.1.3.5. How to tests and generate test reports

To build and run all avaimlable test to get code test coverage and rest api html reports issue following command  

``` bash
./build-and-test.sh
```  

You can now open with your favorite browser index.html coverage report located in  
~/ombs/doc/02-coverage/lcov/index.html

You can also open newman rest api functional index.html test report located in  
~/ombs/doc/02-coverage/newman/api-tests-report.html

## 1.2. Design and code doumentation

Design documentation is available in image format  and html report as well.

Design documentation location

- image capture: ~/ombs/doc/01-Design/01-png-captures/
- html report: ~/ombs/doc/01-Design/02-html-report/index.html
- MagicDraw source file: ~/ombs/doc/01-Design/movieTicketBookingServiceArchitecture.mdzip

Design documentation location

Doxygen code documentation is also automatically generated during build scripts, it is availabe here:  

~/ombs/doc/03-Doxygen/html/index.html

## 1.3. How to run

you can run release version or devug verion for more log verbose

### 1.3.1. run debug version

```bash
./build/bin/ombsserver
```

### 1.3.2. run release version

```bash
./build-release/bin/ombsserver
```

## 1.4. How to use

to use this backedn , please follow next section about its Rest API detailed documentation

## 1.5. OMBS API Documentation

This API allows you to book desried number of seats for a movie show  
The API is available at the folowing base url:  

<http://nicolas-haddad.hd.free.fr:5000>

### 1.5.1. Get all playing movie shows with at least one available seat

Get:<http://nicolas-haddad.hd.free.fr:5000>

#### 1.5.1.1. request example

```curl
curl http://nicolas-haddad.hd.free.fr:5000
````

#### Respsone example

```json
     { 
     "shows" : [ 
          { 
               "_id" : { "$oid" : "61d196632cbef0215d081049" },
               "movieTitle" : "Movie_01" 
          } 
     ] 
     }
```

### 1.5.2. Get all theaters that showing a particular movie and having at least one available seat

Get:<http://nicolas-haddad.hd.free.fr:5000/:movieId>

#### 1.5.2.1. Mandatory path parameters

params

- movieId: movie $oid

params

#### 1.5.2.2. request example

```curl
curl http://nicolas-haddad.hd.free.fr:5000/61d196632cbef0215d081049
````

#### Response example

```json
[
    {
        "theater": {
            "_id": {
                "$oid": "61d2fdbf77f4030e205b9245"
            },
            "theaterName": "Theater_01"
        }
    }
]
```

### 1.5.3. Get all available seats for a given movie in a given theater

Get:<http://nicolas-haddad.hd.free.fr:5000/:movieId/:theaterId>

#### 1.5.3.1. Mandatory path parameters

params

- movieId: movie $oid
- theaterId: theater $oid

params

#### 1.5.3.2. request example

```curl
curl http://nicolas-haddad.hd.free.fr:5000/61d196632cbef0215d081049/61d2fdbf77f4030e205b9245
````

#### Response example

```json
{
    "availableSeatsIds": [
        "61d2fdbf77f4030e205b9250",
        "61d2fdbf77f4030e205b9251",
        "61d2fdbf77f4030e205b9252",
        "61d2fdbf77f4030e205b9253",
        "61d2fdbf77f4030e205b9254",
        "61d2fdbf77f4030e205b9255",
        "61d2fdbf77f4030e205b9256",
        "61d2fdbf77f4030e205b9257",
        "61d2fdbf77f4030e205b9258",
        "61d2fdbf77f4030e205b9259"
    ]
}
```

### 1.5.4. Get all present movies

Get:<http://nicolas-haddad.hd.free.fr:5000/getallmovies>

#### 1.5.4.1. request example

```curl
curl http://nicolas-haddad.hd.free.fr:5000/getallmovies
```

#### 1.5.4.2. Response example

```json
{
    "movies": [
        {
            "_id": {
                "$oid": "61d2fdbf77f4030e205b9244"
            },
            "movieTitle": "Movie_01"
        }
    ]
}
```

### 1.5.5. Get all present theaters

Get:<http://nicolas-haddad.hd.free.fr:5000/getalltheaters>

#### 1.5.5.1. request example

```curl
curl http://nicolas-haddad.hd.free.fr:5000/getalltheaters
```

#### 1.5.5.2. Response example

```json
{
    "theaters": [
        {
            "_id": {
                "$oid": "61d2fdbf77f4030e205b9245"
            },
            "theaterName": "Theater_01"
        }
    ]
}
```

### 1.5.6. Add a new movie

Post:<http://nicolas-haddad.hd.free.fr:5000/addmovie>

#### 1.5.6.1. Mandatory body parameters

you need to supply following parameters in the body in a json document format:

params

- movieTitle: movie title

params

#### 1.5.6.2. Body example

```json
{
    "movieTitle" : "Postman_Movie"
}
```

#### 1.5.6.3. Request example

```curl
curl -X POST http://nicolas-haddad.hd.free.fr:5000/addmovie \
   -d '{"movieTitle" : "Postman_Movie"}'
```

### 1.5.7. Add a new tehater

Post:<http://nicolas-haddad.hd.free.fr:5000/addtheater>

#### 1.5.7.1. Mandatory body parameters

you need to supply following parameters in the body in a json document format:

params

- theaterName : the theater name

params

#### 1.5.7.2. Body example

```json
{
    "theaterName" : "Postman_theater"
}
```

#### 1.5.7.3. Request example

```curl
curl -X POST http://nicolas-haddad.hd.free.fr:5000/addtheater \
   -d '{ "theaterName" : "Postman_theater"}'
```

### 1.5.8. Add movie show

this will add a new show fo a given movie show with 20 available seats in given theater

Post:<http://nicolas-haddad.hd.free.fr:5000/addshow>

#### 1.5.8.1. Mandatory body parameters

you need to supply following parameters in the body in a json document format:

params

- movieId: the movie $oid
- theaterId: the teahter $oid

params

#### 1.5.8.2. Body example

```json
{
    "movieId": "61d196632cbef0215d081049",
    "theaterId": "61d2fdbf77f4030e205b9245"
}
```

#### 1.5.8.3. Request example

```curl
curl -X POST https://reqbin.com/echo/post/json 
   -d '{"movieId": "61d196632cbef0215d081049", "theaterId": "61d2fdbf77f4030e205b9245"}'  
```

### 1.5.9. Book show seats

this will will book all the seats from same show that have seats Id in the boby json array

Post:<http://nicolas-haddad.hd.free.fr:5000/bookseats>

#### 1.5.9.1. Mandatory body parameters

you need to supply following parameters in the body in a json document format:

params

- json array containing all desired seats $oids to book

params

#### 1.5.9.2. Body example

```json
[
    "61d2fdbf77f4030e205b9250",
    "61d2fdbf77f4030e205b9251",
    "61d2fdbf77f4030e205b9252",
    "61d2fdbf77f4030e205b9253",
    "61d2fdbf77f4030e205b9254",
    "61d2fdbf77f4030e205b9255",
    "61d2fdbf77f4030e205b9256",
    "61d2fdbf77f4030e205b9257",
    "61d2fdbf77f4030e205b9258",
    "61d2fdbf77f4030e205b9259"
]
```

#### 1.5.9.3. Request example

```curl
curl -X POST http://nicolas-haddad.hd.free.fr:5000/bookseats \
   -d '["61d2fdbf77f4030e205b9250","61d2fdbf77f4030e205b9251"]'
```
