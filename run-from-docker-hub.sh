#!/bin/bash
# Stop all running containers:
docker stop $(docker ps -a -q)

# Delete all stopped containers:
docker rm $(docker ps -a -q)

# Remove previous image:
docker rmi nicolashaddad/ombs:1.0.0

# pull ombs image from docker hub
docker pull nicolashaddad/ombs:1.0.0

# stopping eventual running local mongodb to free default port 27017
net stop mongodb

# run first mongodb container
docker-compose up -d mongo

# then we can run ombsserver container
docker-compose up -d ombs
