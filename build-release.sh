#!/bin/bash
DIR="build-release"

if ! [ -d "$DIR" ]; 
    then
        mkdir $DIR 
fi

cd $DIR
cmake  -DCMAKE_BUILD_TYPE=Release ..
make
