#!/bin/bash
TOP_DIR=$(pwd)
wget https://github.com/Kitware/CMake/releases/download/v3.20.0/cmake-3.20.0.tar.gz
tar -zxvf cmake-3.20.0.tar.gz
cd cmake-3.20.0
./bootstrap
make
sudo make install
cd $TOP_DIR
#  remove tarchive and extracted folder agter istall
sudo rm -rf cmake-3.20.0
rm cmake-3.20.0.tar.gz
