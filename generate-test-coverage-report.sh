#!/bin/bash
COVERAGE_REPORT_FOLDER=doc/02-coverage/lcov
#clean from previous test coverage
rm -rf $COVERAGE_REPORT_FOLDER/*
lcov --zerocounters --directory ./build/src

# stop any previous running ombsunittests or ombsserver process# stop unit tests process
kill -9 "$(pidof ombsunittests)"
kill -9 "$(pidof ombsserver)"

# launch unit tests
./build/bin/ombsunittests

# generate coverage info file
lcov -c   --directory ./build/src  --output-file $COVERAGE_REPORT_FOLDER/ombs_test_coverage.info --exclude "/usr/*" --exclude "**/include/*"

# generate html report
genhtml $COVERAGE_REPORT_FOLDER/ombs_test_coverage.info --output-directory $COVERAGE_REPORT_FOLDER
