#!/bin/bash
NEWMAN_REPORT_FOLDER=doc/02-coverage/newman

#clean from previous test coverage
rm -rf $NEWMAN_REPORT_FOLDER/*

# Deployment
# newman run https://www.getpostman.com/collections/6a46db05cacfd7ff3d76 --reporters cli,htmlextra\
#  --reporter-htmlextra-export "$NEWMAN_REPORT_FOLDER/api-tests-report.html"
 
# Dev and debug (local)
 newman run https://www.getpostman.com/collections/b47980cd64f932dbe9b5 --reporters cli,htmlextra\
 --reporter-htmlextra-export "$NEWMAN_REPORT_FOLDER/api-tests-report.html"
 