# stopping eventual running local mongodb to free default port 27017
net stop mongodb

# run first mongodb container
docker-compose up -d mongo

# then we can run ombsserver container
docker-compose up -d ombs
