# cleaning build folders
rm -r -fo build
rm -r -fo build-release

# build docker image
docker-compose build
