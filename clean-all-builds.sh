#!/bin/bash
DEBUG_BUILD_DIR="build"

if [ -d "$DEBUG_BUILD_DIR" ]; 
    then
        rm -rf $DEBUG_BUILD_DIR 
fi

RELEASE_BUILD_DIR="build-release"

if [ -d "$RELEASE_BUILD_DIR" ]; 
    then
        rm -rf $RELEASE_BUILD_DIR 
fi

rm -rf doc/03-Doxygen/*
