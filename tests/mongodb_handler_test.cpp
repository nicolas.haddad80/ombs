#include "gtest/gtest.h"
#include "mongodb_handler.hpp"

namespace omsb
{

    class MongoDbHandlerTest : public ::testing::Test
    {

    public:
        std::string movie_01_title = "Movie_01";
        std::string theater_01_title = "Theater_01";

        void SetUp() override
        {
            std::cout << "In Setup" << std::endl;
            std::cout << "Drop all omsb database contents" << std::endl;
            MongoDbHandler mongodb_handler;
            mongodb_handler.DropDatabase();
        }

        void TearDown() override
        {
            std::cout << "in TearDown" << std::endl;
        }

        std::string ExtractMovieTitleFromJson(json::JSON &response)
        {
            return response.at("movies").at(0).at("movieTitle").ToString();
        }

        std::string ExtractTheaterTitleFromJson(json::JSON &response)
        {
            return response.at("theaters").at(0).at("theaterName").ToString();
        }

        std::string ExtractMovieIdFromGetAllMoviesJson(json::JSON &response)
        {
            return response.at("movies").at(0).at("_id").at("$oid").ToString();
        }
        std::string ExtractTheaterIdFromGetAllTheatersJson(json::JSON &response)
        {
            return response.at("theaters").at(0).at("_id").at("$oid").ToString();
        }

        std::string ExtractMovieTitleFromGetAllMovieShowsJson(json::JSON &response)
        {
            return response.at("shows").at(0).at("movieTitle").ToString();
        }

        std::string ExtractTheaterTitleFromGetTheatersByMovie(json::JSON &response)
        {
            return response.at(0).at("theater").at("theaterName").ToString();
        }

    }; // class MongoDbHandlerTest

    TEST_F(MongoDbHandlerTest, Movies)
    {
        MongoDbHandler mongodb_handler;
        json::JSON result = mongodb_handler.GetAllMovies();
        ASSERT_EQ(1, result.size());
        ASSERT_EQ(0, result.at("movies").size());
        ASSERT_TRUE(mongodb_handler.AddMovieToDb(movie_01_title));

        result = mongodb_handler.GetAllMovies();

        ASSERT_EQ(1, result.size());
        ASSERT_EQ(1, result.at("movies").size());
        ASSERT_EQ(movie_01_title, ExtractMovieTitleFromJson(result));
    }

    TEST_F(MongoDbHandlerTest, Theaters)
    {
        MongoDbHandler mongodb_handler;
        json::JSON result = mongodb_handler.GetAllTheaters();
        ASSERT_EQ(1, result.size());
        ASSERT_EQ(0, result.at("theaters").size());
        ASSERT_TRUE(mongodb_handler.AddTheaterToDb(theater_01_title));

        result = mongodb_handler.GetAllTheaters();

        ASSERT_EQ(1, result.size());
        ASSERT_EQ(1, result.at("theaters").size());
        ASSERT_EQ(theater_01_title, ExtractTheaterTitleFromJson(result));
    }

    TEST_F(MongoDbHandlerTest, AddMovieShow)
    {
        MongoDbHandler mongodb_handler;
        json::JSON result = mongodb_handler.GetAllMovieShows();
        ASSERT_EQ(0, result.size());
        // Inserting a movie
        ASSERT_TRUE(mongodb_handler.AddMovieToDb(movie_01_title));
        result = mongodb_handler.GetAllMovies();
        std::string movie_id = ExtractMovieIdFromGetAllMoviesJson(result);

        // Inserting a theater
        ASSERT_TRUE(mongodb_handler.AddTheaterToDb(theater_01_title));
        result = mongodb_handler.GetAllTheaters();
        std::string theater_id = ExtractTheaterIdFromGetAllTheatersJson(result);

        // Adding a show for previous movie in previous theater
        ASSERT_TRUE(mongodb_handler.AddMovieShowToDb(theater_id, movie_id));

        result = mongodb_handler.GetAllMovieShows();
        ASSERT_EQ(1, result.size());
        ASSERT_EQ(1, result.at("shows").size());
        ASSERT_EQ(movie_01_title, ExtractMovieTitleFromGetAllMovieShowsJson(result));
    }

    TEST_F(MongoDbHandlerTest, TheatersByMovie)
    {
        MongoDbHandler mongodb_handler;
        json::JSON result = mongodb_handler.GetAllMovieShows();
        ASSERT_EQ(0, result.size());

        // Inserting a movie
        ASSERT_TRUE(mongodb_handler.AddMovieToDb(movie_01_title));
        result = mongodb_handler.GetAllMovies();
        std::string movie_id = ExtractMovieIdFromGetAllMoviesJson(result);
        // Inserting a theater
        ASSERT_TRUE(mongodb_handler.AddTheaterToDb(theater_01_title));
        result = mongodb_handler.GetAllTheaters();
        std::string theater_id = ExtractTheaterIdFromGetAllTheatersJson(result);

        // Adding a show for previous movie in previous theater
        ASSERT_TRUE(mongodb_handler.AddMovieShowToDb(theater_id, movie_id));
        result = mongodb_handler.GetAllMovieShows();
        ASSERT_EQ(1, result.size());
        result = mongodb_handler.GetTheatersByMovie(movie_id);
        ASSERT_EQ(theater_01_title, ExtractTheaterTitleFromGetTheatersByMovie(result));
    }

    TEST_F(MongoDbHandlerTest, GetSeatsByTheatersByMovie)
    {
        MongoDbHandler mongodb_handler;
        json::JSON result = mongodb_handler.GetAllMovieShows();
        ASSERT_EQ(0, result.size());

        // Inserting a movie
        ASSERT_TRUE(mongodb_handler.AddMovieToDb(movie_01_title));
        result = mongodb_handler.GetAllMovies();
        std::string movie_id = ExtractMovieIdFromGetAllMoviesJson(result);
        // Inserting a theater
        ASSERT_TRUE(mongodb_handler.AddTheaterToDb(theater_01_title));
        result = mongodb_handler.GetAllTheaters();
        std::string theater_id = ExtractTheaterIdFromGetAllTheatersJson(result);
        // Adding a show for previous movie in previous theater
        ASSERT_TRUE(mongodb_handler.AddMovieShowToDb(theater_id, movie_id));

        result = mongodb_handler.GetSeatsByMovieAndByTheater(movie_id, theater_id);
        ASSERT_EQ(1, result.size());
        ASSERT_TRUE(result.hasKey("availableSeatsIds"));
        ASSERT_EQ(20, result.at("availableSeatsIds").size());

#ifndef NDEBUG
        std::cout << result.dump();
#endif
    }

    TEST_F(MongoDbHandlerTest, BookSeats)
    {
        MongoDbHandler mongodb_handler;
        json::JSON result = mongodb_handler.GetAllMovieShows();
        ASSERT_EQ(0, result.size());

        // Inserting a movie
        ASSERT_TRUE(mongodb_handler.AddMovieToDb(movie_01_title));
        result = mongodb_handler.GetAllMovies();
        std::string movie_id = ExtractMovieIdFromGetAllMoviesJson(result);
        // Inserting a theater
        ASSERT_TRUE(mongodb_handler.AddTheaterToDb(theater_01_title));
        result = mongodb_handler.GetAllTheaters();
        std::string theater_id = ExtractTheaterIdFromGetAllTheatersJson(result);
        // Adding a show for previous movie in previous theater
        ASSERT_TRUE(mongodb_handler.AddMovieShowToDb(theater_id, movie_id));

        result = mongodb_handler.GetSeatsByMovieAndByTheater(movie_id, theater_id);

        // Choosing 10 first seats
        std::vector<std::string> seatsIdsList;
        for (int i = 0; i < 10; i++)
        {
            seatsIdsList.emplace_back(result.at("availableSeatsIds").at(i).ToString());
        }

        ASSERT_TRUE(mongodb_handler.BookSeats(seatsIdsList));

        // try to book sames seats
        ASSERT_FALSE(mongodb_handler.BookSeats(seatsIdsList));
    }
} // namespace omsb
