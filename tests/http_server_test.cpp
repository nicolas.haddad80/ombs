#include "gtest/gtest.h"
#include "http_server.hpp"

namespace omsb
{

    class HttpServerTest : public ::testing::Test
    {
    };

    TEST_F(HttpServerTest, HttpServerStart)
    {
        served::multiplexer multiplexer;
        omsb::HttpServer http_server(multiplexer);
        http_server.StartServer();
    }
}
