#!/bin/bash

GTESTS_EXECUTION_DELAY=3s
# Stopping all running services
kill -9 "$(pidof ombsunittests)"
kill -9 "$(pidof ombsserver)"

# run goole unit tests with coverage report
./generate-test-coverage-report.sh&

# waiting fot google unit testsend
sleep $GTESTS_EXECUTION_DELAY

# run rest api fuinctional tests 
./run-newman-rest-api-tests.sh

#Gentely stop  ombsunittests in order to allow for reportes to collect coverage data.
kill -SIGINT "$(pidof ombsunittests)"

# TODO: if in docker then we need to think about starting real server (release server)
